package ru.sberbank.jd;

import ru.sberbank.jd.patterns.Singleton;

public class ApplicationSingleton {
    public static void main(String[] args) {
        // Реализация Логирования
        Singleton test = Singleton.getInstance();
        Singleton test2 = Singleton.getInstance();

        test.add("Добавляем лог через класс: test");
        test2.add("Добавляем лог через класс: test2");

        test.showLog();
    }
}
