package ru.sberbank.jd.patterns;

public class Singleton {
    private static Singleton instance;
    private String log;

    static {
        instance = new Singleton();
        // В этом блоке возможна обработка исключений
    }

    private Singleton () {
        System.out.println("Начало логирования\n");
        this.log = "---------------------------\n    Логирование:";


    }

    public static Singleton getInstance() {
        return instance;
    }

    public void add(String log) {
        this.log += "\n" + log  ;
    }

    public void showLog() {
        System.out.println(this.log);
        System.out.println("---------------------------");
    }
}