package ru.sberbank.jd.patterns.nullobject;

abstract class AbstractEntity {

    public abstract void doSomething();
}
