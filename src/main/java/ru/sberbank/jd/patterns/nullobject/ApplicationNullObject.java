package ru.sberbank.jd.patterns.nullobject;
/*
    Пример структурного шаблона Null Object

    Null Object — это объект с определенным нейтральным («null») поведением.
    Шаблон проектирования Null Object описывает использование таких объектов и их поведение
    (или отсутствие такового).
 */

public class ApplicationNullObject {

    public static void main(String[] args) {
        AbstractEntity realEntity = new RealEntity();
        realEntity.doSomething(); // RealEntity::doSomething

        AbstractEntity unknownEntity = new NullEntity();
        unknownEntity.doSomething(); // no output
    }
}
