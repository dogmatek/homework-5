package ru.sberbank.jd.patterns.nullobject;

class RealEntity extends AbstractEntity {
    @Override
    public void doSomething() {
        System.out.println("RealEntity::doSomething");
    }
}
