package ru.sberbank.jd.patterns.flyweight;
/*
    Пример структурного шаблона Приспособленец (flyweight)
 */

public class Applicationflyweight {
    public static void main (String [] args){
        FlyweightFactory factory = new FlyweightFactory();

        int codes[] = {1,2};
        for (int nextCode : codes){
            Figure figure = factory.getFigure(nextCode);
            figure.ShowDescription();
        }
    }
}
