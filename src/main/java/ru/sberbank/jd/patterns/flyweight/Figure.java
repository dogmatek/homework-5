package ru.sberbank.jd.patterns.flyweight;

public abstract class Figure {
    protected int height;
    protected int width;
    protected int depth;
    public abstract void ShowDescription();
}
