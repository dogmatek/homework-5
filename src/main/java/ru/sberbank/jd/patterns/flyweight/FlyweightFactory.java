package ru.sberbank.jd.patterns.flyweight;

import java.util.HashMap;

public class FlyweightFactory {
    private HashMap<Integer, Figure> figures = new HashMap();

    public Figure getFigure(int characterCode){
        Figure figure = figures.get(characterCode);
        if (figure == null){
            switch (characterCode){
                case 1 : {
                    figure = new Cube();
                    break;
                }
                case 2 : {
                    figure = new Parallelepiped();
                    break;
                }
            }
            figures.put(characterCode, figure);
        }
        return figure;
    }
}
