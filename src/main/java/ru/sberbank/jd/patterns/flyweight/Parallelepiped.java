package ru.sberbank.jd.patterns.flyweight;

public class Parallelepiped extends Figure {
    public Parallelepiped(){
        height = 500;
        width = depth = 100;
    }

    @Override
    public void ShowDescription() {
        System.out.println("height = " + height + " Width = " + width + " depth = " + depth);
    }
}
