package ru.sberbank.jd.patterns.flyweight;

public class Cube extends Figure {
    public Cube(){
        height = width = depth = 100;
    }

    @Override
    public void ShowDescription() {
        System.out.println("height = " + height + " Width = " + width + " depth = " + depth);
    }
}