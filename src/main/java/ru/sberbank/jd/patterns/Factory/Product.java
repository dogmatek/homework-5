package ru.sberbank.jd.patterns.Factory;

public interface Product {
    public void ShowDescription();
}
