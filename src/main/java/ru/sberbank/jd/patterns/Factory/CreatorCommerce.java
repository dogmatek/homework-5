package ru.sberbank.jd.patterns.Factory;

public class CreatorCommerce extends Creator {
    /*
        Создание каласса ProductCommerce
     */
    @Override
    public Product factoryMethod() {
        return new ProductCommerce();
    }
}
