package ru.sberbank.jd.patterns.Factory;

public class ApplicationFactory {
    public static void main(String[] args) {
        Creator creator;

        // creator будет создавать объекты класса CreatorHome -> ProductHome
        creator = new CreatorHome();

        Product product = creator.factoryMethod();
        product.ShowDescription();
        System.out.printf("Created {%s}\n", product.getClass());

        // creator будет создавать объекты класса CreatorHome -> ProductCommerce
        creator = new CreatorCommerce();
        product = creator.factoryMethod();
        product.ShowDescription();
        System.out.printf("Created {%s}\n", product.getClass());

    }
}
