package ru.sberbank.jd.patterns.Factory;

public class CreatorHome extends Creator {
    /*
        Создание класса ProductHome
     */
    @Override
    public Product factoryMethod() {
        return new ProductHome();
    }
}
