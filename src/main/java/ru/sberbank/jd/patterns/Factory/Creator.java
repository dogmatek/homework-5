package ru.sberbank.jd.patterns.Factory;

abstract class Creator {
        public abstract Product factoryMethod();
}
