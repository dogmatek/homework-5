package ru.sberbank.jd.patterns.command;

public interface Command {
    void execute();
}
