package ru.sberbank.jd.patterns.command;
/*
    Команда (англ. Command) — поведенческий шаблон проектирования, используемый при объектно-ориентированном
    программировании, представляющий действие. Объект команды заключает в себе само действие и его параметры.
 */

public class ApplicationCommand {
    public static void main(final String[] arguments) {
        Light lamp = new Light();

        Command switchOn = lamp::turnOn;
        Command switchOff = lamp::turnOff;

        Switch mySwitch = new Switch();
        // именуем команды
        mySwitch.register("Включить", switchOn);
        mySwitch.register("Выключить", switchOff);

        mySwitch.execute("Включить");
        mySwitch.execute("Выключить");
    }
}
